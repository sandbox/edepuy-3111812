CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Credits
 * Requirements
 * Installation
 * Configuration
 * Usage Notes
 * Maintainers


INTRODUCTION
------------

* This module will ALWAYS delete managed files, regardless of any references (or usage).

* Provides a file-delete-link that can be used on page admin/content/files.

* The delete method depends on the value of $config['file.settings.make_unused_managed_files_temporary'].

 * For a full description of the module, visit the project page: https://www.drupal.org/sandbox/edepuy/3111812

 * Recommended: AuditFiles helps cleanup of orphan files: https://www.drupal.org/project/auditfiles

CREDITS
---------
* This module was based on <a href=https://www.drupal.org/project/file_delete >file_delete</a> by Jonathan Eom (https://www.drupal.org/u/jonnyeom) and
and <a href=https://www.drupal.org/project/force_file_delte >force_file_delete</a> by Miles Partnership (https://www.drupal.org/u/ccx105).
Thanks guys!


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Delete method:
    If you want files to be deleted by cron-runs (= current default bheaviour in drupal):
        In settings.php set:
        $config['file.settings']['make_unused_managed_files_temporary'] = TRUE;
        $config['system.file']['temporary_maximum_age'] = 21600;  //= 6 hours
    If you want to delete files immediately:
        In settings.php set:
        $config['file.settings']['make_unused_managed_files_temporary'] = FALSE;

 * Permissions:
    Configure the user permissions in Administration » People » Permissions:
   - Delete files
     Users with this permission will gain access to delete file entities.
   - Delete file forced
     You need this permission to be able to delete with 'force'.

 * Modify VIEW /admin/content/files:
   - Add a Delete link to your Files View in Administration » Structure » Views »
     Files » Edit
     A new "Link to delete File" field should be available.

   - A Delete link should now be visible for files in Administration » Content »
     Files

   - You can also import our improved version for views.view.files (look in config/optional)


USAGE NOTES
-----------

#### Working with Drupal Media
If you added an image to the website as a Drupal Media entity, you will have to
follow these steps.
1. **Important:** Confirm that this Media is not being used in your site.
2. Delete this Media entity in Administration » Content » Media
3. Now you can delete the file in Administration » Content » Files


##### Why is this the case?
Drupal's File Usage system still needs some work. It does not correctly track
all usages within Drupal. Most of the work related to this is being tracked in
[this issue](https://www.drupal.org/project/drupal/issues/2821423)

Specific to Drupal Media, the work is being tracked in
[this issue](https://www.drupal.org/project/drupal/issues/2835840)


MAINTAINERS
-----------

#### Current maintainers:
 * Edgar de Puy (edepuy) - https://www.drupal.org/u/edepuy
